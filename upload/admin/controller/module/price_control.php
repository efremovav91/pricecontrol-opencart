<?php

class ControllerModulePriceControl extends Controller
{
    private $error = array();


    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load->language('module/price_control');
        $this->load->model('module/price_control');
        $this->load->model('catalog/category');

    }

    public function reinstall()
    {
        $this->uninstall();
        $this->install();
        $this->session->data['success'] = $this->language->get('text_success');
        $this->response->redirect($this->url->link('module/price_control', 'token=' . $this->session->data['token'], 'SSL'));
    }

    public function install()
    {
        $this->model_module_price_control->createTable();
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('price_control', array('price_control_status' => 1, 'price_control_version' => $this->model_module_price_control->getVersion()));
    }

    public function backup()
    {
        $this->model_module_price_control->restore();
        $this->session->data['success'] = $this->language->get('text_success');
        $this->response->redirect($this->url->link('module/price_control', 'token=' . $this->session->data['token'], 'SSL'));
    }

    public function uninstall()
    {
        $this->model_module_price_control->deleteTable();
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('price_control', array('price_control_status' => 0, 'price_control_version' => 0));
    }

    public function index()
    {
        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->addStyle('view/stylesheet/pricecontrol.css');
        $this->document->addStyle('view/javascript/jstree/themes/default/style.min.css');
        $this->document->addScript('view/javascript/jstree/jstree.min.js');
        $data['allow_backup'] = $this->model_module_price_control->checkData();
        $data['categories'] = $this->getCategoriesJson(0);
        $data['manufacturers'] = $this->getManufacturers();
        $data['customer_groups'] = $this->getCustomerGroups();
        $data['price_types'] = $this->getPriceTypes();
        $data['math_actions'] = $this->getMathActions();
        $data['version'] = '';
        $data['backup_link'] = $this->url->link('module/price_control/backup', 'token=' . $this->session->data['token'], 'SSL');

        if (!$this->config->get('price_control_status') || (!$this->config->get('price_control_version') || $this->config->get('price_control_version') != $this->model_module_price_control->getVersion())) {
            $this->error['warning'] = $this->language->get('text_not_installed') . " <a href=\"" . $this->url->link('module/price_control/reinstall', 'token=' . $this->session->data['token'], 'SSL') . "\">Переустановить</a>";
        } else {
            $data['version'] = $this->config->get('price_control_version');
        }

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            $this->processChanges();
        }
        $data['heading_title'] = $this->language->get('heading_title') . ($data['version'] ? " " . $data['version'] : "");
        $data['button_close'] = $this->language->get('button_close');
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/account', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['action'] = $this->url->link('module/price_control', 'token=' . $this->session->data['token'], 'SSL');

        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        $this->load->model('design/layout');

        $data['layouts'] = $this->model_design_layout->getLayouts();
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');


        $this->response->setOutput($this->load->view('module/price_control.tpl', $data));
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'module/price_control')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!empty($this->request->post)) {

            if (!isset($this->request->post['priceControl_categories'])) {
                $this->request->post['priceControl_categories'] = array();
            }
            if (!isset($this->request->post['priceControl_manufacturers'])) {
                $this->request->post['priceControl_manufacturers'] = array();
            }
            if (!isset($this->request->post['priceControl_customer_groups'])) {
                $this->request->post['priceControl_customer_groups'] = array();
            }

            if (!isset($this->request->post['priceControl_price_types'])) {
                $this->request->post['priceControl_price_types'][] = ModelModulePriceControl::PRICE_CONTROL_TYPE_BASE;

            } else if (!empty($this->request->post['priceControl_price_types'])) {
                foreach ($this->request->post['priceControl_price_types'] as $priceType) {
                    if (!in_array($priceType, array_keys($this->model_module_price_control->getPriceTypes()))) {
                        $this->error['warning'] = $this->language->get('error_data');
                        break;
                    }
                }
            }

            if (!isset($this->request->post['action']) || !$this->request->post['action']) {
                $this->error['warning'] = $this->language->get('error_data');
            } else {
                if (!in_array($this->request->post['action'], array_keys($this->model_module_price_control->getMathActions()))) {
                    $this->error['warning'] = $this->language->get('error_data');
                } else {
                    switch ($this->request->post['action']) {
                        case ModelModulePriceControl::PRICE_CONTROL_ACTION_ADDICT:
                            $this->request->post['action'] = '+';
                            break;
                        case ModelModulePriceControl::PRICE_CONTROL_ACTION_DEDUCT:
                            $this->request->post['action'] = '-';
                            break;
                        case ModelModulePriceControl::PRICE_CONTROL_ACTION_MULTIPLY:
                            $this->request->post['action'] = '*';
                            break;
                        case ModelModulePriceControl::PRICE_CONTROL_ACTION_DIVIDE:
                            $this->request->post['action'] = '/';
                            break;
                        default:
                            $this->error['warning'] = $this->language->get('error_data');
                    }
                }
            }

            if (!isset($this->request->post['num']) || empty($this->request->post['num'])) {
                $this->error['warning'] = $this->language->get('error_data');
            } else {
                $this->request->post['num'] = str_replace(",", ".", $this->request->post['num']);
                if (!is_numeric(floatval($this->request->post['num'])) || floatval($this->request->post['num']) == 0) {
                    $this->error['warning'] = $this->language->get('error_data');
                }
            }
            if (!isset($this->request->post['unit']) || !$this->request->post['unit']) {
                $this->error['warning'] = $this->language->get('error_data');
            }

        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    protected function processChanges()
    {

        //filter data
        $post_categories = $this->request->post['priceControl_categories'];
        $post_manufacturers = $this->request->post['priceControl_manufacturers'];
        $post_customer_groups = $this->request->post['priceControl_customer_groups'];

        $post_price_types = $this->request->post['priceControl_price_types'];
        $post_action = $this->request->post['action'];
        $post_num = $this->request->post['num'];
        $post_unit = $this->request->post['unit'];
        if ($this->updatePrices($post_price_types, $post_action, $post_num, $post_unit, array(
            'categories' => $post_categories,
            'manufacturers' => $post_manufacturers,
            'customer_groups' => $post_customer_groups))
        ) {
            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($this->url->link('module/price_control', 'token=' . $this->session->data['token'], 'SSL'));
        }

    }

    protected function getCategoriesJson($parent_id)
    {
        $this_cat = $this->model_catalog_category->getCategory($parent_id);
        if (!$this_cat) {
            $this_cat['category_id'] = $parent_id;
            $this_cat['name'] = "Все категории";
        }
        $output = array(
            'id' => 'category' . $this_cat['category_id'],
            'text' => $this_cat['name'],
        );
        $results = $this->model_module_price_control->getCategories($parent_id);
        if ($results) {
            $output['text'] = $this_cat['name'];
            foreach ($results as $result) {
                $output['children'][] = $this->getCategoriesJson($result['category_id']);
            }
        }
        return $output;
    }

    protected function getManufacturers()
    {
        $output = '';
        $this->load->model('catalog/manufacturer');
        $manufacturers = $this->model_catalog_manufacturer->getManufacturers();
        if (!empty($manufacturers)) {
            $output .= '<ul style="list-style:none;">';
            foreach ($manufacturers as $manufacturer) {
                $output .= '<li><input type="checkbox" name="priceControl_manufacturers[]" value="' . $manufacturer['manufacturer_id'] . '" />' . $manufacturer['name'] . '</li>';
            }
            $output .= '</ul>';
        }
        return $output;
    }


    protected function getCustomerGroups()
    {
        $output = '';
        $this->load->model('sale/customer_group');
        $customer_groups = $this->model_sale_customer_group->getCustomerGroups();
        if (!empty($customer_groups)) {
            $output .= '<ul style="list-style:none;">';
            foreach ($customer_groups as $customer_group) {
                $output .= '<li><input type="checkbox" name="priceControl_customer_groups[]" value="' . $customer_group['customer_group_id'] . '" />' . $customer_group['name'] . ' <small>(' . $customer_group['description'] . ')</small></li>';
            }
            $output .= '</ul>';
        }
        return $output;
    }

    protected function getPriceTypes()
    {
        $output = '';
        $priceTypes = $this->model_module_price_control->getPriceTypes();
        if (!empty($priceTypes)) {
            $output .= '<ul style="list-style:none;">';
            foreach ($priceTypes as $priceType => $priceTitle) {
                $output .= '<li><input type="checkbox" name="priceControl_price_types[]" value="' . $priceType . '" />' . $priceTitle . '</li>';
            }
            $output .= '</ul>';

        }
        return $output;
    }

    protected function getMathActions()
    {
        $output = '';
        $mathActions = $this->model_module_price_control->getMathActions();
        if (!empty($mathActions)) {
            $count = 1;
            foreach ($mathActions as $mathAction => $mathTitle) {
                if ($count != 1) {
                    $checked = '';
                    $active = "";
                } else {
                    $checked = 'checked';
                    $active = "active";
                }
                $count++;
                $output .= '<label class="btn btn-default '.$active.'"><input type="radio" ' . $checked . ' name="action" value="' . $mathAction . '" />' . $mathTitle . '</label>';
            }

        }
        return $output;
    }

    protected function updatePrices($price_types, $action, $num, $unit, $filter)
    {
        $this->load->model('module/price_control');
        return $this->model_module_price_control->updatePrices($price_types, $action, $num, $unit, $filter);
    }
}

?>