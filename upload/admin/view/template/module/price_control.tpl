<?php echo $header; ?>
    <script type="text/javascript">
        $(document).ready(function () {
            var data = [<?php echo json_encode($categories);?>];
            jQuery('#category_tree').jstree({ 'core': {
                'data': data
            },
                'plugins': ['wholerow', 'checkbox']
            });

        })
    </script>
    <script type="text/javascript">
        $(document).ready(function () {


            var currentForm;
            $("#pricecontrol_rollback_button").click(function () {
                if (confirm('Откатить последние изменения?')) {
                    var url = $(this).closest('a');
                    location.resolveURL($(url).attr('href'));
                }

            })
            $("#pricecontrol_submit_button").click(function (e) {
                    currentForm = $(this).closest('form');
                    var validate = true;
                    currentForm.find(':input[required]').each(function (i, elem) {
                        if ($(elem).val() == '') {
                            $(elem).css('border-color', 'red');
                            validate = false;
                        } else {
                            $(elem).css('border', 'none');
                        }
                    });
                    if (validate) {
                        if (confirm("Внимательно проверьте все параметры! Неправильное заполнение полей формы может привести к нежелательным последствиям! Выполнить задачу?")) {
                            var selectedCats = $('#category_tree').jstree("get_selected");
                            var selectedCatsIds = [];
                            $.each(selectedCats, function (i, elem) {
                                selectedCatsIds.push(elem.match(/\d+/));
                            });

                            $.each(selectedCatsIds, function (i, selectedCatsId) {
                                $('<input />').attr('type', 'hidden')
                                    .attr('name', 'priceControl_categories[]')
                                    .attr('value', selectedCatsId)
                                    .appendTo($(currentForm));
                            });

                            $(currentForm).submit();

                        }
                    } else {
                        alert("Не все обязательные поля заполнены корректно. Пожалуйста, проверьте введенные данные");
                        e.preventDefault();
                    }
                    return false;
                }
            );
        })
    </script>

<?php echo $column_left; ?>
    <div id="content">
        <div class="page-header">
            <div class="container-fluid">
                <div class="pull-right">
                    <button type="submit" form="pricecontrol-form" data-toggle="tooltip"
                            title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i>
                    </button>
                    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>"
                       class="btn btn-default"><i class="fa fa-reply"></i></a></div>
                <h1><?php echo $heading_title; ?></h1>
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <?php if ($error_warning) { ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <?php } ?>
            <?php if ($success) { ?>
                <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <?php } ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
                </div>
                <div class="panel-body">
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data"
                          id="pricecontrol-form"
                          class="form-horizontal">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-where" class="selected"
                                                  title="Где изменять">Фильтр</a></li>
                            <li><a data-toggle="tab" href="#tab-what"
                                   title="Что изменять">Цены</a></li>
                            <li><a data-toggle="tab" href="#tab-how"
                                   title="Как изменять">Формула и действия</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-where">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <p class="help text-muted">Если для какого-либо параметра фильтра не будет
                                            отмечено ни
                                            одного
                                            значения,
                                            в
                                            процессе изменения цен параметр не будет учитываться. Это равносильно выбору
                                            всех
                                            возможных
                                            значений параметра.
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-3">
                                        <ul class="nav nav-pills nav-stacked">
                                            <li class="active"><a data-toggle="tab" href="#where-cats"><i
                                                        class="fa fa-list"></i>
                                                    Категории</a></li>
                                            <li><a data-toggle="tab" href="#where-manufacturers"><i
                                                        class="fa fa-android"></i> Производители</a></li>
                                            <li><a data-toggle="tab" href="#where-customers"><i class="fa fa-group"></i>
                                                    Группы покупателей</a></a></li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-12 col-md-9">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="where-cats">
                                                <h3><i class="fa fa-list"></i> Категории</h3>

                                                <div class="panel-body">
                                                    <div id="category_tree"></div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="where-manufacturers">
                                                <h3><i class="fa fa-android"></i> Производители</h3>

                                                <div class="panel-body">
                                                    <?php if ($manufacturers)
                                                        echo $manufacturers;
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="where-customers">
                                                <h3><i class="fa fa-group"></i> Группы покупателей</h3>

                                                <div class="panel-body">
                                                    <?php if ($customer_groups)
                                                        echo $customer_groups;
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="tab-what">
                                <p class="help">Выберите, какие значения цен товаров необходимо изменить.
                                    <b>Внимание!</b>
                                    Если не будет выбрано ни одного значения, изменение цен затронет основную
                                    цену
                                    товара!
                                    Будьте внимательны!</p>

                                <div class="categories_list">
                                    <?php if ($price_types)
                                        echo $price_types;
                                    ?>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-how">
                                <div class="col-sm-6">
                                    <h2>Формула</h2>

                                    <div class="container-fluid">
                                        <div class="form-group">
                                            <label>Математическое действие</label>
                                            <br />

                                            <div class="btn-group" data-toggle="buttons">
                                                <?php if ($math_actions)
                                                    echo $math_actions;
                                                ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Число</label>
                                            <br />
                                            <input required class="action_input_field form-control" type="text"
                                                   name="num"
                                                   placeholder="Введите число" />
                                        </div>
                                        <div class="field_units form-group">
                                            <label>Ед. измерения</label>

                                            <div class="btn-group" data-toggle="buttons">
                                                <label class="btn btn-default active"><input checked
                                                                                             class="form-control"
                                                                                             type="radio" name="unit"
                                                                                             value="<?php echo ModelModulePriceControl::PRICE_CONTROL_UNIT_PERCENT; ?>" />%</label>
                                                <label class="btn btn-default"><input class="form-control" type="radio"
                                                                                      name="unit"
                                                                                      value="<?php echo ModelModulePriceControl::PRICE_CONTROL_UNIT_NUMBER; ?>" />число</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <h2>Действия</h2>

                                    <div class="container-fluid">
                                        <div class="form-group row">
                                            <button class="btn btn-success" id="pricecontrol_submit_button"
                                                    type="submit">
                                                Выполнить
                                            </button>
                                        </div>
                                        <div class="form-group row">
                                            <a href="<?php echo $backup_link; ?>">
                                                <button <?php echo(!$allow_backup ? "disabled" : ""); ?>
                                                    class="btn btn-warning" id="pricecontrol_rollback_button"
                                                    type="button">Восстановить предыдущие
                                                    значения
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


<?php echo $footer; ?>