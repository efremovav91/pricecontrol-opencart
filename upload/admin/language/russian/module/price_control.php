<?php
// Heading
$_['heading_title']       = 'Price Control - Групповое управление ценами';
// Text
$_['text_module']         = 'Модули';
$_['text_success']        = 'Операция выполнена успешно!';
$_['text_not_installed']   = 'Модуль управления ценами установлен неправильно! Для корректной работы переустановите модуль';

$_['text_content_top']    = 'Верх страницы';
$_['text_content_bottom'] = 'Низ страницы';
$_['text_column_left']    = 'Левая колонка';
$_['text_column_right']   = 'Правая колонка';

// Entry
$_['entry_layout']        = 'Макет:';
$_['entry_position']      = 'Расположение:';
$_['entry_status']        = 'Статус:';
$_['entry_sort_order']    = 'Порядок сортировки:';

// Error
$_['error_permission']    = 'У Вас нет прав для управления этим модулем!';
$_['error_data']    = 'Ошибка выполнения операции! Пожалуйста, правильно заполните все поля формы!';

?>